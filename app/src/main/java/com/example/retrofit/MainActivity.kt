package com.example.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.retrofit.repository.Repository

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    private lateinit var textView: TextView
    private lateinit var button: Button
    private lateinit var number_editText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById<TextView>(R.id.textView) as TextView
        button = findViewById<Button>(R.id.button) as Button
        number_editText = findViewById<EditText>(R.id.number_editText) as EditText

        val repository = Repository()
        val viewModelFactory = MainViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
//        viewModel.getPost()
//        viewModel.myResponse2.observe(this, Observer { response ->
//            if (response.isSuccessful){
//                Log.d("Response", response.body()?.userId.toString())
//                Log.d("Response", response.body()?.id.toString())
//                Log.d("Response", response.body()?.title!!)
//                textView.text = response.body()?.title!!
//                Log.d("Response", response.body()?.body!!)
//            }else{
//                Log.d("Response", response.errorBody().toString())
//                textView.text = response.code().toString()
//            }
//        })

        val options: HashMap<String, String> = HashMap()
        options["_sort"] = "id"
        options["_order"] = "desc"

        button.setOnClickListener {
            val myNumber = number_editText.text.toString()
            viewModel.getCustomPosts2(Integer.parseInt(myNumber),options )
            viewModel.getCustomPost4.observe(this, Observer { response ->
                if (response.isSuccessful){
                    textView.text = response.body().toString()
                    response.body()?.forEach {
                        Log.d("Response", it.userId.toString())
                        Log.d("Response", it.id.toString())
                        Log.d("Response", it.title)
                        Log.d("Response", it.body)
                        Log.d("Response", "------------")
                    }
                }else{
                    textView.text = response.code().toString()
                }
            })
        }



    }
}

// https://jsonplaceholder.typicode.com/posts/1